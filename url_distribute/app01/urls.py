from django.conf.urls import url
from app01 import views

urlpatterns = [
    url(r'^reg/', views.reg),
    url(r'^index/', views.index),
    url(r'^files/', views.files),
    url(r'^myclass/', views.MyClass.as_view()),
]