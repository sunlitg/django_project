from django.shortcuts import render, HttpResponse
from django.http import JsonResponse

# Create your views here.
def reg(request):
    return HttpResponse('app01: reg')

import json

def index(request):
    user_dict = {
        'username': 'one',
        'password': '111',
        'hobby': '乒乓'
    }
    return JsonResponse(user_dict, json_dumps_params={'ensure_ascii': False})

def files(request):
    if request.method == 'POST':
        #print(request.FILES)
        #<MultiValueDict: {'file': [<InMemoryUploadedFile: 2-data.json (application/json)>]}>
        fileobj = request.FILES.get('file')
        filename = fileobj.name
        filepath = 'templates/' + filename
        with open(filepath, mode='wb') as f:
            for line in fileobj.chunks():
                f.write(line)
    return render(request, 'file.html')

from django.views import View

class MyClass(View):
    def get(self, request):
        return render(request, 'file.html')
    
    def post(self, request):
        return HttpResponse('post请求')