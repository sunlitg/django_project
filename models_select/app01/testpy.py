import os
import sys

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "models_select.settings")
    import django

    django.setup()
    # 如下是测试代码
    from app01 import models

    from django.db.models import Max, Min, Sum, Count, Avg

    from django.db.models import F


    # res = models.Book.objects.filter(saled_count__gt=F('stock'))
    # print(res) # <QuerySet [<Book: 孟子>, <Book: 大学>]>
    # res = models.Book.objects.update(price = F('price') + 200)
    # print(res)
    # from django.db.models.functions import Concat
    # from django.db.models import Value
    # res = models.Book.objects.update(title=Concat(F('title') , Value('爆款')))
    # print(res) # 4
    # from django.db.models import Q
    # res = models.Book.objects.filter(~Q(saled_count__gt=100))
    # print(res) # <QuerySet [<Book: 论语爆款>]>
    # res = models.Book.objects.filter(saled_count__gt=100, price__lt=424)
    # res = models.Book.objects.filter(Q(saled_count__gt=100), Q(price__lt=424))
    # print(res) # <QuerySet [<Book: 孟子爆款>]>
    # res = models.Book.objects.filter()
    # res = models.Book.objects.filter(Q(saled_count__gt=100)|Q(price__lt=424))
    # print(res) # <QuerySet [<Book: 论语爆款>, <Book: 孟子爆款>, <Book: 大学爆款>, <Book: 中庸爆款>]>
    # q = Q()
    # q.connector = 'or'
    # q.children.append(('saled_count__gt', 100))
    # q.children.append(('price__lt', 424))
    # res = models.Book.objects.filter(q)
    # print(res) # <QuerySet [<Book: 论语爆款>, <Book: 孟子爆款>, <Book: 大学爆款>, <Book: 中庸爆款>]>
    # from django.db import transaction
    # with transaction.atomic():
    #     print('sql1')
    #     print('sql2')
    #     print('with下的sql都属于一个事务')
    # print('')
