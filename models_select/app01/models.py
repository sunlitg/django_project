from django.db import models


# Create your models here.


class User(models.Model):
    name = models.CharField(max_length=32, verbose_name='名字')
    age = models.IntegerField()
    register_time = models.DateField(verbose_name='注册时间')  # 年月日
    '''
    两种时间格式的说明
    DateField      年月日
    DateTimeField  年月日时分秒
       他们都有两个重要参数
       auto_now: 每次操作数据的时候 都会将事件字段的值更新为当前时间
       auto_now_add: 在创建(插入)数据的时候会写下当前的时间 后续不会变 除非认为修改
    '''

    def __str__(self):
        return '对象: %s' % self.name


class Book(models.Model):
    title = models.CharField(max_length=32, verbose_name='书籍名称')
    price = models.DecimalField(max_digits=8, decimal_places=2, verbose_name='价格')
    publish_date = models.DateField(auto_now_add=True, verbose_name='出版日期')
    stock = models.IntegerField(default=1000, verbose_name='库存量')
    saled_count = models.IntegerField(default=1000, verbose_name='已售量')
    # 书籍对出版社是一对多
    publish = models.ForeignKey(to='Publish')
    # 书籍对作者是多对多的关系
    authors = models.ManyToManyField(to='Author')

    def __str__(self):
        return self.title


class Publish(models.Model):
    name = models.CharField(max_length=32, verbose_name='出版社名称')
    addr = models.CharField(max_length=64, verbose_name='出版社地址')
    email = models.EmailField(verbose_name='邮件地址')


class Author(models.Model):
    name = models.CharField(max_length=32, verbose_name='作者姓名')
    age = models.IntegerField(verbose_name='年龄')
    # 作者和详情表是一对一关系
    author_detail = models.OneToOneField(to='AuthorDetail')


class AuthorDetail(models.Model):
    phone = models.BigIntegerField(verbose_name='手机号码')
    addr = models.CharField(max_length=64, verbose_name='住址')
