from django.shortcuts import redirect, render,HttpResponse,reverse
from app01 import models
# Create your views here.

def userlist(request):
    user_queryset = models.User.objects.all()
    return render(request, 'userlist.html', locals())

def modify_user(request, id):
    user_obj = models.User.objects.filter(id=id).first()
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        models.User.objects.filter(id=id).update(username=username, password=password)
        return redirect('/userlist/')
    return render(request, 'modify.html', locals())

def del_user(request, id):
    models.User.objects.filter(id=id).delete()
    return redirect('/userlist/')

def home(request):
    return render(request, 'home.html')

def funck(request, year):
    # 有名分组反向解析传参方式一
    print(reverse('kkk', args=(1234,)))
    # 有名分组反向解析传参方式二
    print(reverse('kkk', kwargs={'year': 1234}))
    return HttpResponse('funck')

