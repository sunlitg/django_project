from django import template

register = template.Library()

# 自定义过滤器(自定义过滤器最多两个参数)
@register.filter(name='sumtool')
def mysum(v1, v2):
    return v2 + v1

@register.simple_tag(name='tagtool')
def inde(a, b , c):
    return '%s-%s-%s' %(a, b, c)

@register.inclusion_tag('left_menu.html')
def left(n):
    data = [ '第{}个元素'.format(i) for i in range(n) ]
    return locals()