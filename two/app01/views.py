from django.shortcuts import render, HttpResponse, redirect
from app01 import models

def login(request):
    # print(request.method, type(request.method)) #POST <class 'str'>
    # print(request.POST, type(request.POST)) # <QueryDict: {'username': ['admin'], 'password': ['admin']}> <class 'django.http.request.QueryDict'>
    # print(request.POST.get('username'), type(request.POST.get('username'))) #admin <class 'str'>
    # print(request.POST.getlist('username'), type(request.POST.getlist('username'))) #['admin'] <class 'list'>
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user_obj = models.User.objects.filter(username=username, password=password).first()
        if user_obj:
            return HttpResponse('登录成功')
        return HttpResponse('用户名或密码错误')
    return render(request,'login.html') 

def reg(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        ## 方法一:
        # res = models.User.objects.create(username=username, password=password)
        # print(res)
        # 方法二: 
        user_obj = models.User(username=username, password=password)
        user_obj.save()
        return HttpResponse('注册成功')
    return render(request, 'reg.html')

def userlist(request):
    # 第一种查询所有的方法 不建议使用 因为filter的原本含义就是筛选某个字段 这里没有筛选
    #user_obj = models.User.objects.filter()
    # 第二种 推荐使用 语义明确
    user_queryset = models.User.objects.all()
    return render(request, 'userlist.html', locals())

def edit(request):
    edit_id = request.GET.get('edit_id')
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user_obj = models.User.objects.filter(id=edit_id).first()
        user_obj.username = username
        user_obj.password = password
        user_obj.save()
        return redirect('/userlist/')
    user_obj = models.User.objects.filter(id=edit_id).first()
    return render(request, 'modify.html', locals())

def delete_user(request):
    user_id = request.GET.get('user_id')
    models.User.objects.filter(id=user_id).delete()
    return redirect('/userlist/')